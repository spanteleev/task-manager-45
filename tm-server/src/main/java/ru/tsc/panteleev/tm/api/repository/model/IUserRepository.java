package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.User;
import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void set(@NotNull Collection<User> users);

    List<User> findAll();

    User findById(@NotNull String id);

    void removeById(@NotNull String id);

    void clear();

    long getSize();

    User findByLogin(@NotNull String login);

    User findByEmail(@NotNull String email);

}
