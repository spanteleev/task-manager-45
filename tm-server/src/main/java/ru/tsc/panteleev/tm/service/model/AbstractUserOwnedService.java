package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.model.IUserOwnedService;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
