package ru.tsc.panteleev.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@Nullable UserDTO user) {
        this.user = user;
    }

}
